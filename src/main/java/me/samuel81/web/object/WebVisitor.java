package me.samuel81.web.object;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public abstract class WebVisitor {
	
	protected List<String> cookies = new ArrayList<>();
	
	protected CloseableHttpClient httpClient;
	protected CookieStore cookieStore;
	
	protected String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18362";
	
	protected String COOKIES_LINK = "";
	protected String LINK = "";
	
	protected boolean HAVE_CAPTCHA = false;

	public WebVisitor() throws Exception {
		cookieStore = new BasicCookieStore();
		httpClient = HttpClients.custom().setDefaultCookieStore(cookieStore).build();
	}
	
    protected void close() throws IOException {
        httpClient.close();
    }
    
    public abstract String check(User user) throws Exception;

	public abstract String sendPost(User user) throws Exception;

	public abstract String sendGet() throws Exception;
	
	public abstract byte[] getCaptcha() throws Exception;

	public List<String> getCookies() {
		return cookies;
	}

	public void setCookies(List<String> cookies) {
		this.cookies = cookies;
	}

}
