package me.samuel81.web.object;

import java.util.concurrent.TimeUnit;

import me.samuel81.web.BankType;
import me.samuel81.web.visitor.BcaWebVisitor;

public class User {

	private String ID;
	private String PWD;

	private BankType bank;

	private String cachedMutasi = "";
	private long latestFetch = -1;

	private WebVisitor wv;

	public User(String ID, String PWD) {
		this.ID = ID;
		this.PWD = PWD;
		try {
			wv = new BcaWebVisitor();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getID() {
		return ID;
	}

	public String getPassword() {
		return PWD;
	}

	public BankType getBankType() {
		return bank;
	}

	public String getLatestMutasi() {
		if (latestFetch == -1 || TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - latestFetch) >= 6) {
			try {
				fetch();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return cachedMutasi;
	}

	public void fetch() throws Exception {
		cachedMutasi = wv.check(this);
		latestFetch = System.currentTimeMillis();
	}

}
