package me.samuel81.web.controller;

import javax.validation.constraints.NotBlank;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import me.samuel81.web.UserManager;
import me.samuel81.web.object.WebVisitor;
import me.samuel81.web.visitor.BriWebVisitor;

@RestController
@RequestMapping("/mutasi/*")
public class ControllerMutasi {
	
	@GetMapping(value = "/test",
			  produces = MediaType.IMAGE_PNG_VALUE)
	public @ResponseBody byte[] test() throws Exception {
		WebVisitor wv = new BriWebVisitor();
		return wv.getCaptcha();
	}
	
	@GetMapping("/bri")
	public String bangsat() throws Exception {
		WebVisitor wv = new BriWebVisitor();
		return wv.sendGet()/* wv.sendPost(UserManager.getUser("SAMUELHI3008")) */;
	}

	@GetMapping(value = "/bca")
	public String bca(@RequestParam @NotBlank String acc) throws Exception {
		if(UserManager.getUser(acc) == null)
			return "NO USER WITH ID "+acc;
		return UserManager.getUser(acc).getLatestMutasi();
	}

}