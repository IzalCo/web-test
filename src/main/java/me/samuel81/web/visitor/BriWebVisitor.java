package me.samuel81.web.visitor;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import me.samuel81.web.object.User;
import me.samuel81.web.object.WebVisitor;

public class BriWebVisitor extends WebVisitor {

	private String COOKIES_LINK = "https://ib.bri.co.id/ib-bri/";
	private String LINK = "https://ib.bri.co.id/ib-bri/Homepage.html";

	private String[] DATE;
	
	private String TOKEN;

	public BriWebVisitor() throws Exception {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDateTime now = LocalDateTime.now();
		DATE = dtf.format(now).split("/");
		HAVE_CAPTCHA = true;
	}

	public String check(User user) throws Exception {
		sendGet();
		sendPost(user);
		HttpPost conn = new HttpPost("https://ibank.klikbca.com/accountstmt.do?value(actions)=acctstmtview");

		// Acts like a browser
		conn.addHeader("Host", "ibank.klikbca.com");
		conn.addHeader("Referer", "https://ibank.klikbca.com/accountstmt.do?value(actions)=acct_stmt");
		conn.addHeader("User-Agent", USER_AGENT);
		conn.addHeader("Accept", "text/html, application/xhtml+xml, application/xml; q=0.9, */*; q=0.8");
		conn.addHeader("Accept-Encoding", "gzip, deflate, br");
		conn.addHeader("Accept-Language", "en-ID");
		conn.addHeader("Cache-Control", "max-age=0");
		for (String cookie : this.cookies) {
			conn.addHeader("Cookie", cookie.split(";", 1)[0]);
		}
		conn.addHeader("Connection", "keep-alive");
		conn.addHeader("Upgrade-Insecure-Requests", "1");
		conn.addHeader("Content-Type", "application/x-www-form-urlencoded");

		List<NameValuePair> urlParameters = new ArrayList<>();
		urlParameters.add(new BasicNameValuePair("value(D1)", "0"));
		urlParameters.add(new BasicNameValuePair("value(endDt)", DATE[0]));
		urlParameters.add(new BasicNameValuePair("value(endMt)", DATE[1]));
		urlParameters.add(new BasicNameValuePair("value(endYr)", DATE[2]));
		urlParameters.add(new BasicNameValuePair("value(fDt)", ""));
		urlParameters.add(new BasicNameValuePair("value(r1)", "1"));
		urlParameters.add(new BasicNameValuePair("value(startDt)", DATE[0]));
		urlParameters.add(new BasicNameValuePair("value(startMt)", DATE[1]));
		urlParameters.add(new BasicNameValuePair("value(startYr)", DATE[2]));
		urlParameters.add(new BasicNameValuePair("value(submit1)", "Lihat+Mutasi+Rekening"));
		urlParameters.add(new BasicNameValuePair("value(tDt)", ""));
		UrlEncodedFormEntity params = new UrlEncodedFormEntity(urlParameters);
		conn.setEntity(new UrlEncodedFormEntity(urlParameters));
		// conn.addHeader("Content-Length", "" + params.getContentLength());

		try (CloseableHttpClient httpClient = HttpClients.createDefault();
				CloseableHttpResponse response = httpClient.execute(conn)) {
			System.out.println("\nSending 'POST' request to URL : " + LINK);
			System.out.println("Post parameters : " + params.toString());
			System.out.println("Response Code : " + response.getStatusLine());
			String res = EntityUtils.toString(response.getEntity());
			return res;
		}
	}

	public String sendPost(User user) throws Exception {
		File file = new File("E:\\Desktop\\Kerjaan\\eclipse\\test.png");
		FileUtils.writeByteArrayToFile(file, getCaptcha());
		sendGet();
		HttpPost conn = new HttpPost(LINK);

		conn.addHeader("Host", "ib.bri.co.id");
		conn.addHeader("User-Agent", USER_AGENT);
		conn.addHeader("Accept", "text/html, application/xhtml+xml, application/xml; q=0.9, */*; q=0.8");
		conn.addHeader("Accept-Encoding", "gzip, deflate, br");
		conn.addHeader("Accept-Language", "en-ID");
		conn.addHeader("Cache-Control", "max-age=0");
		conn.addHeader("Referer", "https://ib.bri.co.id/ib-bri/");
		conn.addHeader("Cache-Control", "max-age=0");
		for (String cookie : this.cookies) {
			conn.addHeader("Cookie", cookie.split(";", 1)[0]);
		}
		conn.addHeader("Connection", "keep-alive");
		conn.addHeader("Upgrade-Insecure-Requests", "1");
		conn.addHeader("Content-Type", "application/x-www-form-urlencoded");

		List<NameValuePair> urlParameters = new ArrayList<>();
		urlParameters.add(new BasicNameValuePair("csrf_token_newib", TOKEN));
		urlParameters.add(new BasicNameValuePair("j_code", "1296"));
		urlParameters.add(new BasicNameValuePair("j_language", "in_ID"));
		urlParameters.add(new BasicNameValuePair("j_plain_username", user.getID()));
		urlParameters.add(new BasicNameValuePair("j_plain_password", ""));
		urlParameters.add(new BasicNameValuePair("j_password", user.getPassword()));
		urlParameters.add(new BasicNameValuePair("j_username", user.getID()));
		urlParameters.add(new BasicNameValuePair("preventAutoPass", ""));
		UrlEncodedFormEntity params = new UrlEncodedFormEntity(urlParameters);
		conn.setEntity(new UrlEncodedFormEntity(urlParameters));
		// conn.addHeader("Content-Length", "" + params.getContentLength());

		try (CloseableHttpResponse response = httpClient.execute(conn)) {
			System.out.println("\nSending 'POST' request to URL : " + LINK);
			System.out.println("Post parameters : " + params.toString());
			System.out.println("Response Code : " + response.getStatusLine());

			String result = EntityUtils.toString(response.getEntity());
			return result;
		}
	}

	public String sendGet() throws Exception {
		
		HttpGet conn = new HttpGet(COOKIES_LINK);

		// act like a browser
		conn.addHeader("Host", "ib.bri.co.id");
		conn.addHeader("User-Agent", USER_AGENT);
		conn.addHeader("Accept", "text/html, application/xhtml+xml, application/xml; q=0.9, */*; q=0.8");
		conn.addHeader("Accept-Encoding", "gzip, deflate, br");
		conn.addHeader("Accept-Language", "en-ID");
		conn.addHeader("Upgrade-Insecure-Requests", "1");
		conn.addHeader("Connection", "Keep-Alive");
		conn.addHeader("Cache-Control", "max-age=0");
		if (cookies != null) {
			for (String cookie : this.cookies) {
				conn.addHeader("Cookie", cookie.split(";", 1)[0]);
			}
		}
		try (CloseableHttpResponse response = httpClient.execute(conn)) {
			System.out.println("\nSending 'GET' request to URL : " + COOKIES_LINK);
			System.out.println("Response Code : " + response.getStatusLine().toString());

			for (Header str : response.getHeaders("Set-Cookie")) {
				cookies.add(str.getValue());
			}
			
			// read content
			String content = EntityUtils.toString(response.getEntity());

			// find string between: <select name="fromAccountID"> and </select>
			String begin = "<input type=\"hidden\" name=\"csrf_token_newib\" value=\"", end = "\" />";
			int beginPos = content.indexOf(begin), endPos = content.indexOf(end, beginPos);
			TOKEN = content.substring(beginPos+begin.length(), endPos);
			return TOKEN;
		}

	}

	@Override
	public byte[] getCaptcha() throws Exception {
		HttpGet conn = new HttpGet("https://ib.bri.co.id/ib-bri/login/captcha");

		// act like a browser
		conn.addHeader("Host", "ib.bri.co.id");
		conn.addHeader("User-Agent", USER_AGENT);
		conn.addHeader("Referer", "https://ib.bri.co.id/ib-bri/");
		conn.addHeader("Accept", "image/png, image/svg+xml, image/*; q=0.8, */*; q=0.5");
		conn.addHeader("Accept-Encoding", "gzip, deflate, br");
		conn.addHeader("Accept-Language", "en-ID");
		conn.addHeader("Upgrade-Insecure-Requests", "1");
		conn.addHeader("Cache-Control", "max-age=0");
		conn.addHeader("Connection", "Keep-Alive");
		if (cookies != null) {
			for (String cookie : this.cookies) {
				conn.addHeader("Cookie", cookie.split(";", 1)[0]);
			}
		}
		try (CloseableHttpResponse response = httpClient.execute(conn)) {
			System.out.println("\nSending 'GET' request to URL : https://ib.bri.co.id/ib-bri/login/captcha");
			System.out.println("Response Code : " + response.getStatusLine().toString());
			return EntityUtils.toByteArray(response.getEntity());
		}
	}

}
