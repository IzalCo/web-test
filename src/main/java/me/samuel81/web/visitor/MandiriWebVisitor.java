package me.samuel81.web.visitor;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import me.samuel81.web.object.User;
import me.samuel81.web.object.WebVisitor;

public class MandiriWebVisitor extends WebVisitor {

	private String LINK = "https://ib.bankmandiri.co.id/retail/Login.do";
	private String COOKIES_LINK = "https://ib.bankmandiri.co.id/retail/Login.do?action=form&lang=in_ID";

	private String[] DATE;

	public MandiriWebVisitor() throws Exception {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDateTime now = LocalDateTime.now();
		DATE = dtf.format(now).split("/");
	}

	public String check(User user) throws Exception {
		sendGet();
		sendPost(user);
		
		HttpGet httpGet;
		HttpResponse resp;
		List<NameValuePair> formParams;
		//UrlEncodedFormEntity entity;

		/*
		 * // open login page httpGet = new HttpGet(COOKIES_LINK); resp =
		 * httpClient.execute(httpGet); EntityUtils.consume(resp.getEntity());
		 */

		/*
		 * // login formParams = new ArrayList<NameValuePair>(); formParams.add(new
		 * BasicNameValuePair("action", "result")); formParams.add(new
		 * BasicNameValuePair("userID", user.getID())); formParams.add(new
		 * BasicNameValuePair("password", user.getPassword())); formParams.add(new
		 * BasicNameValuePair("image.x", "0")); formParams.add(new
		 * BasicNameValuePair("image.y", "0")); entity = new
		 * UrlEncodedFormEntity(formParams, "UTF-8"); httpPost = new HttpPost(LINK);
		 * httpPost.setEntity(entity); resp = httpClient.execute(httpPost);
		 * EntityUtils.consume(resp.getEntity());
		 * 
		 * // check if login success? String successUrl =
		 * "https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward"; Header[]
		 * locationHeaders = resp.getHeaders("Location"); if (locationHeaders.length ==
		 * 0 || locationHeaders[0].getValue().equals(successUrl) == false) {
		 * System.out.println("Login failed!"); return "ID / PASSWORD SALAH!"; }
		 */

		// open page to obtain account list
		httpGet = new HttpGet("https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do?action=form");
		resp = httpClient.execute(httpGet);

		// read content
		StringBuilder buffer = new StringBuilder();
		InputStream instream = resp.getEntity().getContent();
		BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
		String line;
		while ((line = reader.readLine()) != null) {
			buffer.append(line);
		}

		// find string between: <select name="fromAccountID"> and </select>
		String begin = "<select name=\"fromAccountID\">", end = "</select>";
		int beginPos = buffer.indexOf(begin) + begin.length(), endPos = buffer.indexOf(end, beginPos);
		String accountStr = buffer.substring(beginPos, endPos);
		// account values is in each of <option value="{acc_id}">{acc_no}</option>
		ArrayList<String> accountList = new ArrayList<String>();
		begin = "<option value=\"";
		end = "\"";
		beginPos = 0;
		endPos = 0;
		while (true) {
			if ((beginPos = accountStr.indexOf(begin, endPos)) == -1)
				break;
			beginPos += begin.length();
			if ((endPos = accountStr.indexOf(end, beginPos)) == -1)
				break;
			// first account id will empty string "Silahkan Pilih"
			if (beginPos != endPos) {
				String accId = accountStr.substring(beginPos, endPos);
				accountList.add(accId);
			}
		}

		// get account statement
		formParams = new ArrayList<NameValuePair>();
		formParams.add(new BasicNameValuePair("action", "result"));
		formParams.add(new BasicNameValuePair("fromAccountID", accountList.get(0))); // <--- baca account pertama saja
		formParams.add(new BasicNameValuePair("searchType", "R"));
		formParams.add(new BasicNameValuePair("fromDay", DATE[0])); // <--- tanggal from & to, cukup jelas ya
		formParams.add(new BasicNameValuePair("fromMonth", DATE[1]));
		formParams.add(new BasicNameValuePair("fromYear", DATE[2]));
		formParams.add(new BasicNameValuePair("toDay",  DATE[0]));
		formParams.add(new BasicNameValuePair("toMonth", DATE[1]));
		formParams.add(new BasicNameValuePair("toYear", DATE[2]));
		formParams.add(new BasicNameValuePair("sortType", "Date"));
		formParams.add(new BasicNameValuePair("orderBy", "ASC"));
		//entity = new UrlEncodedFormEntity(formParams, "UTF-8");
		/*
		 * httpPost = new
		 * HttpPost("https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do");
		 * httpPost.setEntity(entity); resp = httpClient.execute(httpPost);
		 */

		// read content
		buffer = new StringBuilder();
		instream = resp.getEntity().getContent();
		reader = new BufferedReader(new InputStreamReader(instream));
		while ((line = reader.readLine()) != null) {
			buffer.append(line + "\n");
		}

		// find string between: <!-- Start of Item List --> and <!-- End of Item List
		// -->
		begin = "<!-- Start of Item List -->";
		end = "<!-- End of Item List -->";
		beginPos = buffer.indexOf(begin) + begin.length();
		endPos = buffer.indexOf(end, beginPos);
		String accountStmt = buffer.substring(beginPos, endPos);
		System.out.println(accountStmt);

		// logout
		httpGet = new HttpGet("https://ib.bankmandiri.co.id/retail/Logout.do?action=result");
		resp = httpClient.execute(httpGet);
		EntityUtils.consume(resp.getEntity());
		return accountStmt;

	}

	public String sendPost(User user) throws Exception {
		HttpPost conn = new HttpPost(LINK);

		/*
		 * // open login page httpGet = new HttpGet(COOKIES_LINK); resp =
		 * httpClient.execute(httpGet); EntityUtils.consume(resp.getEntity());
		 */
		
		conn.addHeader("Host", "ib.bankmandiri.co.id");
		conn.addHeader("Referer", "https://ib.bankmandiri.co.id/retail/Login.do?action=form&lang=in_ID");
		conn.addHeader("User-Agent", USER_AGENT);
		conn.addHeader("Accept", "text/html, application/xhtml+xml, application/xml; q=0.9, */*; q=0.8");
		conn.addHeader("Accept-Encoding", "gzip, deflate, br");
		conn.addHeader("Accept-Language", "en-ID");
		conn.addHeader("Cache-Control", "max-age=0");
		conn.addHeader("Upgrade-Insecure-Requests", "1");
		for (String cookie : this.cookies) {
			conn.addHeader("Cookie", cookie.split(";", 1)[0]);
		}
		conn.addHeader("Connection", "keep-alive");
		conn.addHeader("Upgrade-Insecure-Requests", "1");
		conn.addHeader("Content-Type", "application/x-www-form-urlencoded");
		
		// prepare login param
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("action", "result"));
		params.add(new BasicNameValuePair("userID", user.getID()));
		params.add(new BasicNameValuePair("password", user.getPassword()));
		params.add(new BasicNameValuePair("image.x", "0"));
		params.add(new BasicNameValuePair("image.y", "0"));
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, "UTF-8");
		conn.setEntity(entity);
		
		try (CloseableHttpClient httpClient = HttpClients.createDefault();
				CloseableHttpResponse response = httpClient.execute(conn)) {
			System.out.println("\nSending 'POST' request to URL : " + LINK);
			System.out.println("Post parameters : " + params.toString());
			System.out.println("Response Code : " + response.getStatusLine());
			
			// check if login success?
			String successUrl = "https://ib.bankmandiri.co.id/retail/Redirect.do?action=forward";
			Header[] locationHeaders = response.getHeaders("Location");
			if (locationHeaders.length == 0 || locationHeaders[0].getValue().equals(successUrl) == false) {
				System.out.println("Login failed!");
				return "ID / PASSWORD SALAH!";
			}
		}
		return "SUCCESS";
	}

	public String sendGet() throws Exception {
		HttpGet conn = new HttpGet(COOKIES_LINK);
		
		// act like a browser
		conn.addHeader("Host", "ib.bankmandiri.co.id");
		conn.addHeader("User-Agent", USER_AGENT);
		conn.addHeader("Accept", "text/html, application/xhtml+xml, application/xml; q=0.9, */*; q=0.8");
		conn.addHeader("Accept-Encoding", "gzip, deflate, br");
		conn.addHeader("Accept-Language", "en-ID");
		conn.addHeader("Upgrade-Insecure-Requests", "1");
		conn.addHeader("Connection", "Keep-Alive");
		conn.addHeader("Cache-Control", "max-age=0");
		if (cookies != null) {
			for (String cookie : this.cookies) {
				conn.addHeader("Cookie", cookie.split(";", 1)[0]);
			}
		}
		try (CloseableHttpResponse response = httpClient.execute(conn)) {
			System.out.println("\nSending 'GET' request to URL : " + COOKIES_LINK);
			System.out.println("Response Code : " + response.getStatusLine().toString());
			for (Header str : response.getHeaders("Set-Cookie")) {
				cookies.add(str.getValue());
			}
			return EntityUtils.toString(response.getEntity());
		}
	}

	@Override
	public byte[] getCaptcha() throws Exception {
		return null;
	}

}
