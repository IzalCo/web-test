package me.samuel81.web.visitor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import me.samuel81.web.UserManager;
import me.samuel81.web.object.User;
import me.samuel81.web.object.WebVisitor;

public class BcaWebVisitor extends WebVisitor {

	private String COOKIES_LINK = "https://ibank.klikbca.com/login.jsp";
	private String LINK = "https://ibank.klikbca.com/authentication.do";
	
	private String[] DATE;

	public BcaWebVisitor() throws Exception {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDateTime now = LocalDateTime.now();
		DATE = dtf.format(now).split("/");
	}

	public String check(User user) throws Exception {
		sendGet();
		sendPost(user);
		HttpPost conn = new HttpPost("https://ibank.klikbca.com/accountstmt.do?value(actions)=acctstmtview");

		// Acts like a browser
		conn.addHeader("Host", "ibank.klikbca.com");
		conn.addHeader("Referer", "https://ibank.klikbca.com/accountstmt.do?value(actions)=acct_stmt");
		conn.addHeader("User-Agent", USER_AGENT);
		conn.addHeader("Accept", "text/html, application/xhtml+xml, application/xml; q=0.9, */*; q=0.8");
		conn.addHeader("Accept-Encoding", "gzip, deflate, br");
		conn.addHeader("Accept-Language", "en-ID");
		conn.addHeader("Cache-Control", "max-age=0");
		for (String cookie : this.cookies) {
			conn.addHeader("Cookie", cookie.split(";", 1)[0]);
		}
		conn.addHeader("Connection", "keep-alive");
		conn.addHeader("Upgrade-Insecure-Requests", "1");
		conn.addHeader("Content-Type", "application/x-www-form-urlencoded");

		List<NameValuePair> urlParameters = new ArrayList<>();
		urlParameters.add(new BasicNameValuePair("value(D1)", "0"));
		urlParameters.add(new BasicNameValuePair("value(endDt)", DATE[0]));
		urlParameters.add(new BasicNameValuePair("value(endMt)", DATE[1]));
		urlParameters.add(new BasicNameValuePair("value(endYr)", DATE[2]));
		urlParameters.add(new BasicNameValuePair("value(fDt)", ""));
		urlParameters.add(new BasicNameValuePair("value(r1)", "1"));
		urlParameters.add(new BasicNameValuePair("value(startDt)", DATE[0]));
		urlParameters.add(new BasicNameValuePair("value(startMt)", DATE[1]));
		urlParameters.add(new BasicNameValuePair("value(startYr)", DATE[2]));
		urlParameters.add(new BasicNameValuePair("value(submit1)", "Lihat+Mutasi+Rekening"));
		urlParameters.add(new BasicNameValuePair("value(tDt)", ""));
		UrlEncodedFormEntity params = new UrlEncodedFormEntity(urlParameters, "UTF-8");
		conn.setEntity(params);
		//conn.addHeader("Content-Length", "" + params.getContentLength());

		try (CloseableHttpResponse response = httpClient.execute(conn)) {
			System.out.println("\nSending 'POST' request to URL : " + LINK);
			System.out.println("Post parameters : " + params.toString());
			System.out.println("Response Code : " + response.getStatusLine());
			String res = EntityUtils.toString(response.getEntity());
			return res;
		}
	}

	public String sendPost(User user) throws Exception {

		HttpPost conn = new HttpPost(LINK);

		conn.addHeader("Host", "ibank.klikbca.com");
		conn.addHeader("User-Agent", USER_AGENT);
		conn.addHeader("Accept", "text/html, application/xhtml+xml, application/xml; q=0.9, */*; q=0.8");
		conn.addHeader("Accept-Encoding", "gzip, deflate, br");
		conn.addHeader("Accept-Language", "en-ID");
		conn.addHeader("Cache-Control", "max-age=0");
		for (String cookie : this.cookies) {
			conn.addHeader("Cookie", cookie.split(";", 1)[0]);
		}
		conn.addHeader("Connection", "keep-alive");
		conn.addHeader("Upgrade-Insecure-Requests", "1");
		conn.addHeader("Content-Type", "application/x-www-form-urlencoded");

		List<NameValuePair> urlParameters = new ArrayList<>();
		urlParameters.add(new BasicNameValuePair("value(actions)", "login"));
		urlParameters.add(new BasicNameValuePair("value(user_id)", user.getID()));
		urlParameters.add(new BasicNameValuePair("value(user_ip)", UserManager.IP_ADDRESS));
		urlParameters.add(new BasicNameValuePair("value(browser_info)", USER_AGENT));
		urlParameters.add(new BasicNameValuePair("value(mobile)", "false"));
		urlParameters.add(new BasicNameValuePair("value(pswd)", user.getPassword()));
		urlParameters.add(new BasicNameValuePair("value(Submit)", "LOGIN"));
		UrlEncodedFormEntity params = new UrlEncodedFormEntity(urlParameters, "UTF-8");
		conn.setEntity(params);
		//conn.addHeader("Content-Length", "" + params.getContentLength());

		try (CloseableHttpResponse response = httpClient.execute(conn)) {
			System.out.println("\nSending 'POST' request to URL : " + LINK);
			System.out.println("Post parameters : " + params.toString());
			System.out.println("Response Code : " + response.getStatusLine());

			String result = EntityUtils.toString(response.getEntity());
			return result.contains("login kembali setelah") ? result : "";
		}
	}

	public String sendGet() throws Exception {

		HttpGet conn = new HttpGet(COOKIES_LINK);

		// act like a browser
		conn.addHeader("Host", "ibank.klikbca.com");
		conn.addHeader("User-Agent", USER_AGENT);
		conn.addHeader("Accept", "text/html, application/xhtml+xml, application/xml; q=0.9, */*; q=0.8");
		conn.addHeader("Accept-Encoding", "gzip, deflate, br");
		conn.addHeader("Accept-Language", "en-ID");
		conn.addHeader("Upgrade-Insecure-Requests", "1");
		conn.addHeader("Connection", "Keep-Alive");
		if (cookies != null) {
			for (String cookie : this.cookies) {
				conn.addHeader("Cookie", cookie.split(";", 1)[0]);
			}
		}
		try (CloseableHttpResponse response = httpClient.execute(conn)) {
			System.out.println("\nSending 'GET' request to URL : " + COOKIES_LINK);
			System.out.println("Response Code : " + response.getStatusLine().toString());
			// HttpEntity entity = response.getEntity();

			/*
			 * if (entity != null) { // return it as a String String result =
			 * EntityUtils.toString(entity); System.out.println(result); }
			 */
			// Get the response cookies
			// setCookies(conn.getHeaderFields().get("Set-Cookie"));
			for (Header str : response.getHeaders("Set-Cookie")) {
				cookies.add(str.getValue());
			}
			return EntityUtils.toString(response.getEntity());
		}
		
	}

	@Override
	public byte[] getCaptcha() throws Exception {
		return null;
	}

}
