package me.samuel81.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import me.samuel81.web.object.User;

public class UserManager {
	
	private static Map<String, User> users = new HashMap<>();
	
	public static String IP_ADDRESS;
	
	static {
		try {
			init();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void init() throws IOException {
		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
		
		/**
		 * Fetching workstation IP address
		 */
		URL whatismyip = new URL("http://checkip.amazonaws.com");
		BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
		String ip = in.readLine(); // get the IP as a String
		IP_ADDRESS = ip;
		
		/**
		 * Testing user
		 */
		User user = new User("SAMUELHI3008", "063232");
		UserManager.addUser(user);
	}
	
	public static void addUser(User user) {
		users.put(user.getID(), user);
	}
	
	public static User getUser(String id) {
		return users.get(id);
	}

}
