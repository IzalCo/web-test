package me.samuel81.web;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebApplication {

	public static void main(String[] args) throws IOException {

		SpringApplication.run(WebApplication.class, args);
	}

}
